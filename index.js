// req axios

// Path: index.js

const axios = require("axios");
const cheerio = require("cheerio");
const fs = require("fs");

async function ScrapEinheiten() {
  const listOfEinheiten = [];
  const url =
    "https://www.thw.de/SiteGlobals/Forms/Suche/Umkreissuche/Umkreissuche_Formular.html";
  const response = await axios.get(url);
  const $ = cheerio.load(response.data);
  // Select by CSS Classes c-facet__list js-facet-item is-hidden
  const einheiten = $(
    "#content > div:nth-child(2) > div > div:nth-child(2) > div.l-facet-toggle.js-simple-toggle > div > div > div > div > div:nth-child(4) > div > ul"
  ).children();

  for (let i = 0; i < einheiten.length; i++) {
    const element = einheiten[i];
    //console.log(element.children);

    // Parse child a elements
    const fullname = $(element).children("a").text();
    const link = $(element).children("a").attr("href");

    // Parse fullname Count in a seperate variable. The Text has the format like "Fachgruppe Schwere Bergung (B) (10Treffer)" , "Fachgruppe Sprengen (10Treffer)" , Fachgruppe Räumen Typ C (58Treffer)
    const fullnameCount = fullname.match(/\d+/g);
    // Match the (NNTreffer) and replace it with nothing. The NN stands for 2 digits

    const trimedName = fullname.replace(/\(\d+Treffer\)/g, "").trim();

    const Einheit_id = link
      .replace(
        "SiteGlobals/Forms/Suche/Umkreissuche/Umkreissuche_Formular.html?cl2Categories_Einheit=",
        ""
      )
      .replace("#searchresults", "");

    const einheit_obj = {
      id: Einheit_id,
      name: trimedName,
      count: fullnameCount,
    };

    listOfEinheiten.push(einheit_obj);
  }
  return listOfEinheiten;
}

// Get filtered Dst and response with geojson
async function GetFilteredDst(unit_id) {
  const url = `https://www.thw.de/SiteGlobals/Forms/Suche/Umkreissuche/Umkreissuche_Formular.html?cl2Categories_Einheit=${unit_id}&map=1`;

  const response = await axios.get(url);
  return response.data;
}

// Get all Dst and response with geojson
async function GetAllDst() {
  const url =
    "https://www.thw.de/SiteGlobals/Forms/Suche/Umkreissuche/Umkreissuche_Formular.html?ambit_distance=25&map=1";
  const response = await axios.get(url);
  return response.data;
}

async function main() {
  //   const ovs = await GetAllOvs();
  //   console.log(ovs);
  const data = await ScrapEinheiten();

  // Save as unitid.json
  fs.writeFileSync("dst/unitid.json", JSON.stringify(data));

  // Get all Dsts and write to file
  const ovs = await GetAllDst();

  // Transform the coordinates array in each feature from string values to float values
  ovs.features.forEach((element) => {
    element.geometry.coordinates[0] = parseFloat(
      element.geometry.coordinates[0]
    );
    element.geometry.coordinates[1] = parseFloat(
      element.geometry.coordinates[1]
    );
  });

  // Get filtered Dsts by unitid and write each to file
  for (let i = 0; i < data.length; i++) {
    const element = data[i];
    console.log("Try get filtered DST by ", element);
    const dst = await GetFilteredDst(element.id);

    // Add the unit to ovs as a property identify by title
    dst.features.forEach((filterddst) => {
      ovs.features.find((ov) => {
        if (ov.properties.title === filterddst.properties.title) {
          // If units array is not defined, create it
          if (!ov.properties.units) {
            ov.properties.units = [];
          }
          ov.properties.units.push(element.name);
        }
      });
    });

    fs.writeFileSync(
      `dst/seperated/dst_${element.id}.json`,
      JSON.stringify(dst)
    );
  }
  fs.writeFileSync("dst/ovs.json", JSON.stringify(ovs));
}

main();
